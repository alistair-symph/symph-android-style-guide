## Symph Android Guide

List of guidelines that we use at [Symph](http://www.sym.ph/) when developing for the Android platform.

* [Android Code Style](https://bitbucket.org/alistair-symph/symph-android-style-guide/src/1016720f1c5b7c4375399fefe6cc0f46beb50544/Android%20Code%20Style.md?at=master&fileviewer=file-view-default)

* Architecture Guide

Found some errors or want to add? Fork it and send a pull request.

## License

```
#!text

Copyright 2016 Symph

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```