## Android Guidelines
-----

The style guide is meant to keep our code consistent and easy-to-read between projects. It keeps code quality and organization at a high level, while enabling us to keep writing spectacular code.

## 1. Project Guidelines

#### 1.1 Project Structure

When contributing work, the project should maintain the following structure:

```
#!text
androidTest/
main/
test/
```

**androidTest** - directory containing functional tests

**main** - directory containing application code

**test** - directory containing unit tests

The structure of the project should remain as defined above whenever you are modifying or adding new features. Using this structure allows us to keep the application code seperated from any test-related code.

#### 1.2 File Naming

#### 1.2.1 Class Files
Any classes that you define should be named using UpperCamelCase

`MainActivity` `BaseFragment` `NetworkUtil` `UserResponseCallback`

Any classes extending an Android framework component should always end with the component name

`LoginActivity` `ProfileFragment` `UpdateDialog` `PieChartView`

#### 1.2.2 Resource Files
When naming resources files, make sure to use lower case letters and underscores.

`activity_main` `fragment_base` `item_profile` `menu_settings`

Using prefixes may help developers determine the file where they are used.  It is also easier to read and search for Java files with their respective xml layout file.

- `activity_` for `Activity` subclasses
- `fragment_` for `Fragment`s
- `item_` for list item layout

#### 1.2.3 Drawable
Drawable resource files should be named using the ic_ prefix along with the size and color of the asset. For example, white accept icon sized at 24dp would be named:

`ic_accept_24dp_white`

while a black cancel icon with 48dp size would be name:

`ic_cancel_48dp_black`

This is more recognisable by its name, size, and colour.

#### 1.2.4 Layout

When naming the layout files, they should be named with the name of Android Component being prefixed.

`AppCompatActivity` as `activity_main`
`Fragment` as `fragment_profile`
`AlertDialog` as `dialog_rate`
`View` as `view_piechart`
`AdapterView` as `item_tweet`

With this, it is easier for us to search files and much easier to identify which file uses particular layout.

#### 1.2.5 Menu

Same with 1.2.5, Menu files are prefixed with `menu_`.

#### 1.2.6 Values
Values files should be in plural form

`strings.xml` `styles.xml` `dimens.xml` `colors.xml` `attrs.xml`

## 2. Code Guidelines

#### 2.1 Java Language

#### 2.1.1 Never catch generic exceptions (Pokemon Exception Handling)

"Gotta catch em all" is a bad idea and is very dangerous because it means that Exceptions you never expected get caught in application-level error handling. It obscures the failure handling properties of your code, meaning if someone adds a new type of Exception in the code you're calling, the compiler won't help you realise you need to handle the error differently. In most cases you shouldn't be handling different types of exception the same way.

```
#!java
public void throwPokeballTo(String pokemonId) {
    try {
        someComplicatedIOFunction();        // may throw IOException
        someComplicatedParsingFunction();   // may throw ParsingException
        someComplicatedSecurityFunction();  // may throw SecurityException
        // phew, made it all the way
    } catch (Exception e) {                 // I'll just catch all exceptions
        handleError(e);                     // with one generic handler!
    }
}
```

Consider this following scenario:

> "A man gets shot.

> He holds his breath and has enough strength to take a bus.

> 10 miles later the man gets off of the bus, walks a couple of blocks and dies."

When the police gets to the body, they don't have a clue of what has just happened. They may have eventually but it is much harder.

Better way:

> "A man gets shot and he dies instantly, and the body lies exactly where the murder just happened."

When the police arrives, all the evidence is in place.

If something can be done with the specific exception, that's where it should be handled.

[source](http://stackoverflow.com/a/921583/1076574)

#### 2.1.2 Never ignore exceptions

Avoid not handling exceptions in a correct manner:

```
#!java
public void setServerPort(String value) {
    try {
        serverPort = Integer.parseInt(value);
    } catch (NumberFormatException e) { }
}
```

Do not do this. While you may think your code will never encounter this error condition or that it is not important to handle it, ignoring exceptions as above creates mines in your code for someone else to trigger some day. You must handle every Exception in your code in a principled way:

Let the caller of this method handle it:

```
#!java
public void setServerPort(String value) throws NumberFormatException {
    serverPort = Integer.parseInt(value);
}
```

Handling it gracefully:

```
#!java
public void setServerPort(String value) {
    try {
        serverPort = Integer.parseInt(value);
    } catch(NumberFormatException e) {
        serverPort = 80; // sets server port to default
    }
}
```

#### 2.2 Java Style

#### 2.2.1 Use whitespaces

Operators should be surrounded by a whitespace character
```
#!java
a = (b + c) * d; // NOT: a=(b+c)*d
```

Commas should be followed by a whitespace character.
```
#!java
fooBar(a, b, c, d);  // NOT: fooBar(a,b,c,d);
```

Semicolons in `for-loop` statements should be followed by a whitespace character.
```
#!java
for(int i = 0; i <= 42; i++) {  // NOT: for(int i=0;i<=42;i++) {
}
```

Always put a whitespace character before every opening curly brace.
```
#!java
public class FooBar {  // NOT: public class FooBar{
}
```

Newlines - never use it after opening braces. Closing braces go on a new line.
```
#!java
public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Foo foo = new Foo();
    foo.setProperty(value);

    Bar bar = new Bar();
}
```

Indentation - Always use spaces(4 spaces), **NOT** tabs

#### 2.2.2 Comments
In general, the use of comments should be minimised by making the code self-documenting by appropriate name choices and an explicit logical structure.

##### Block Comments
Block comments are indented at the same level as the surrounding code. For multi-line comments, subsequent lines must start with `*` aligned with the `*` on the previous line.
```
#!java
/*
 * This is        // And so
 * okay.          // is this.
 */
```

##### TODO
Use TODO comments for code that is temporary, a short-term solution, or good-enough but not perfect. TODOs should include the string TODO in all caps, followed by a colon:
```
#!java
// TODO: use dependency injection
```

##### JavaDoc
Every class or a non-trivial public method must contain a Javadoc comment with at least one sentence describing what the class or method does.
```
#!java
/**
  * Updates the state of all UI elements on the CallCard, based on the
  * current state of the phone.
  */
public void updateState(CallManager cm) {
}
```

#### 2.2.3 Functions

##### Chaining
When chaining, never chain two or multiple functions in one line.
```
#!java
// We advise doing this.
User user = new User.Builder()
        .setFirstname("John")
        .setLastname("Doe")
        .setAge(60)
        .build();

// AVOID DOING THIS
User user = new User.Builder().setFirstname("John").setLastname("Doe").setAge(60).build();
```

#### 2.2.4 Field definition and naming
Fields should be defined at the top of the file and they should follow the naming rules listed below.

* Fields should be named using lowerCamelCase.
* Non-public, non-static field names start with m.
* Static field names start with s.
* Static final fields (constants) are ALL_CAPS_WITH_UNDERSCORES.
* Field names that do not reveal intention should not be used.

```
#!java
public class MyClass {
    public int publicField;
    private int mPrivateField;
    protected int mProtectedField;
    int mField;
    public static final int INT_CONSTANT = 101;
    private static MyClass sSingleton
    
    public MyClass(int field) {
        this.mField = field;
    }

    // AVOID DOING THIS
    int e = 1;
}
```

#### 2.2.5 Use Standard Brace Style
Braces do not go on their own line; they go on the same line as the code before them:

```
#!java
class MyClass {
    int func() {
        if (something) {
            // ...
        } else if (somethingElse) {
            // ...
        } else {
            // ...
        }
    }
}
```

We require braces around the statements for a conditional. Exception: If the entire conditional (the condition and the body) fit on one line, you may (but are not obligated to) put it all on one line. For example, this is acceptable:

```
#!java
if (condition) {
    body();
}
```

and this is acceptable:

```
#!java
if (condition) body();
```

but this is not acceptable:

```
#!java
if (condition)
    body();  // bad!
```

#### 2.3 XML Style

#### 2.4 Test Style

## 3. Gradle Guidelines 

sources:
[Android Code Style Rules](http://source.android.com/source/code-style.html), 
[Google Java Style Guide](https://google.github.io/styleguide/javaguide.html), 
[Raizlabs Android Style](https://github.com/Raizlabs/Raizlabs-Android-Style/blob/master/Android%20Code%20Style.md)
[Buffer](https://github.com/bufferapp/android-guidelines/blob/master/project_style_guidelines.md)
